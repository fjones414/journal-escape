<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
        <style type="text/css">
        
                .container {
                    text-align: center;
                    width: 600px;
                    }
                    
                #homePageContainer {
                    margin-top: 100px;
                }    
                
                #containerLoggedInPage {
                    margin-top: 60px;
                }
        
                html{ 
                      background: linear-gradient(rgba(0,0,0,0.55), rgba(0,0,0,0.55)), url(background.jpg) no-repeat center center fixed;
                      -webkit-background-size: cover;
                      -moz-background-size: cover;
                      -o-background-size: cover;
                      background-size: cover;
                    }
                    
                body{
                        background: none;
                    }
                    
                h1{
                    color: #f2f2f2;
                    font-weight: bold;
                }    
                
                #logInForm{
                    display: none;
                }
                    
                #toggle1, #toggle2 {
                    color: #ccffcc;
                }
                
                .intro{
                    color: #f2f2f2;
                }
                
                .headerP{
                    color: #ccffcc;
                }
                
                .clickOptions{
                    color: white;
                    margin-bottom: 7px;
                }
                
                #diary {
                    width: 100%;
                    height: 100%;
                    background: #f5f5ef;
                }
                
                .navbar-brand{
                    font-weight: bold;
                }
                
        </style>

    <title>My Thoughts</title>
  </head>
